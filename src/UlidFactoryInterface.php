<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use PhpExtended\Factory\FactoryInterface;

/**
 * UlidFactoryInterface interface file.
 *
 * This factory creates Ulid objects.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Factory\FactoryInterface<UlidInterface>
 */
interface UlidFactoryInterface extends FactoryInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Factory\FactoryInterface::create()
	 */
	public function create() : UlidInterface;
	
}
