<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use DateTimeInterface;
use Stringable;

/**
 * UlidInterface interface file.
 * 
 * This interface represents an ULID (Universally Unique Lexicographically
 * Sortable Identifier)
 * 
 * @author Anastaszor
 */
interface UlidInterface extends Stringable
{
	
	/**
	 * The datetime represented by the timestamp bits.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateTime() : DateTimeInterface;
	
	/**
	 * The 48 bits of timestamp.
	 * 
	 * @return integer
	 */
	public function getTimestamp() : int;
	
	/**
	 * The 40 first bits of randomness.
	 * 
	 * @return integer
	 */
	public function getRandomnessUp() : int;
	
	/**
	 * The 40 lower bits of randomness.
	 * 
	 * @return integer
	 */
	public function getRandomnessDown() : int;
	
}
