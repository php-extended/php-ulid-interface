<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ulid-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ulid;

use PhpExtended\Parser\ParserInterface;

/**
 * UlidParserInterface interface file.
 *
 * This interface is a parser to get ulid objects.
 *
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<UlidInterface>
 */
interface UlidParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
